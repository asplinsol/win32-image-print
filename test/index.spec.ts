import { readFileSync } from 'fs';
import { join } from 'path';
import { cwd } from 'process';
import { printImage } from '../src';
import { getPrinter, disposePrinter, getPaperSize } from '../src/gdiAPI';

describe('index', () => {
  describe('printTest', () => {
    let hndl: number;
    it('should give window handle', () => {
      hndl = getPrinter('Microsoft Print To PDF');
      expect(hndl).toBeGreaterThan(0);
    });
    it('should retrieve physical page size', () => {
      const size = getPaperSize(hndl);
      expect(size).toHaveProperty('width');
      expect(size).toHaveProperty('height');
    });
    it('should dispose handle', () => {
      expect(disposePrinter(hndl)).toBeTruthy();
    });
    it('should print png image', async () => {
         const filePath = join(cwd(), 'outPic.png');
         const pngBuffer = readFileSync(filePath);
        const jobNo = await printImage(pngBuffer, 'Microsoft Print To PDF');
        expect(jobNo).toBeGreaterThan(0);
    })
  });
});
