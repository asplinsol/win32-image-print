export declare const LPCSTR = 'string*';
export declare const HANDLE = 'uint32';
export enum MAPMODE {
  TEXT = 1,
  LOMETRIC = 2,
  HIMETRIC = 3,
  LOENGLISH = 4,
  HIENGLISH = 5,
  TWIPS = 6,
  ISOTROPIC = 7,
  ANISOTROPIC = 8,
}
export enum DeviceCap {
  /// <summary>
  /// Device driver version
  /// </summary>
  DRIVERVERSION = 0,
  /// <summary>
  /// Device classification
  /// </summary>
  TECHNOLOGY = 2,
  /// <summary>
  /// Horizontal size in millimeters
  /// </summary>
  HORZSIZE = 4,
  /// <summary>
  /// Vertical size in millimeters
  /// </summary>
  VERTSIZE = 6,
  /// <summary>
  /// Horizontal width in pixels
  /// </summary>
  HORZRES = 8,
  /// <summary>
  /// Vertical height in pixels
  /// </summary>
  VERTRES = 10,
  /// <summary>
  /// Number of bits per pixel
  /// </summary>
  BITSPIXEL = 12,
  /// <summary>
  /// Number of planes
  /// </summary>
  PLANES = 14,
  /// <summary>
  /// Number of brushes the device has
  /// </summary>
  NUMBRUSHES = 16,
  /// <summary>
  /// Number of pens the device has
  /// </summary>
  NUMPENS = 18,
  /// <summary>
  /// Number of markers the device has
  /// </summary>
  NUMMARKERS = 20,
  /// <summary>
  /// Number of fonts the device has
  /// </summary>
  NUMFONTS = 22,
  /// <summary>
  /// Number of colors the device supports
  /// </summary>
  NUMCOLORS = 24,
  /// <summary>
  /// Size required for device descriptor
  /// </summary>
  PDEVICESIZE = 26,
  /// <summary>
  /// Curve capabilities
  /// </summary>
  CURVECAPS = 28,
  /// <summary>
  /// Line capabilities
  /// </summary>
  LINECAPS = 30,
  /// <summary>
  /// Polygonal capabilities
  /// </summary>
  POLYGONALCAPS = 32,
  /// <summary>
  /// Text capabilities
  /// </summary>
  TEXTCAPS = 34,
  /// <summary>
  /// Clipping capabilities
  /// </summary>
  CLIPCAPS = 36,
  /// <summary>
  /// Bitblt capabilities
  /// </summary>
  RASTERCAPS = 38,
  /// <summary>
  /// Length of the X leg
  /// </summary>
  ASPECTX = 40,
  /// <summary>
  /// Length of the Y leg
  /// </summary>
  ASPECTY = 42,
  /// <summary>
  /// Length of the hypotenuse
  /// </summary>
  ASPECTXY = 44,
  /// <summary>
  /// Shading and Blending caps
  /// </summary>
  SHADEBLENDCAPS = 45,

  /// <summary>
  /// Logical pixels inch in X
  /// </summary>
  LOGPIXELSX = 88,
  /// <summary>
  /// Logical pixels inch in Y
  /// </summary>
  LOGPIXELSY = 90,

  /// <summary>
  /// Number of entries in physical palette
  /// </summary>
  SIZEPALETTE = 104,
  /// <summary>
  /// Number of reserved entries in palette
  /// </summary>
  NUMRESERVED = 106,
  /// <summary>
  /// Actual color resolution
  /// </summary>
  COLORRES = 108,

  // Printing related DeviceCaps. These replace the appropriate Escapes
  /// <summary>
  /// Physical Width in device units
  /// </summary>
  PHYSICALWIDTH = 110,
  /// <summary>
  /// Physical Height in device units
  /// </summary>
  PHYSICALHEIGHT = 111,
  /// <summary>
  /// Physical Printable Area x margin
  /// </summary>
  PHYSICALOFFSETX = 112,
  /// <summary>
  /// Physical Printable Area y margin
  /// </summary>
  PHYSICALOFFSETY = 113,
  /// <summary>
  /// Scaling factor x
  /// </summary>
  SCALINGFACTORX = 114,
  /// <summary>
  /// Scaling factor y
  /// </summary>
  SCALINGFACTORY = 115,

  /// <summary>
  /// Current vertical refresh rate of the display device (for displays only) in Hz
  /// </summary>
  VREFRESH = 116,
  /// <summary>
  /// Horizontal width of entire desktop in pixels
  /// </summary>
  DESKTOPVERTRES = 117,
  /// <summary>
  /// Vertical height of entire desktop in pixels
  /// </summary>
  DESKTOPHORZRES = 118,
  /// <summary>
  /// Preferred blt alignment
  /// </summary>
  BLTALIGNMENT = 119,
}
export enum MAPMODE {
  MM_TEXT = 0x0001,
  MM_LOMETRIC = 0x0002,
  MM_HIMETRIC = 0x0003,
  MM_LOENGLISH = 0x0004,
  MM_HIENGLISH = 0x0005,
  MM_TWIPS = 0x0006,
  MM_ISOTROPIC = 0x0007,
  MM_ANISOTROPIC = 0x0008,
}
export enum DIBColors {
  DIB_RGB_COLORS = 0x00,
  DIB_PAL_COLORS = 0x01,
  DIB_PAL_INDICES = 0x02,
}
export enum TernaryRasterOperations {
  SRCCOPY = 0x00cc0020,
  SRCPAINT = 0x00ee0086,
  SRCAND = 0x008800c6,
  SRCINVERT = 0x00660046,
  SRCERASE = 0x00440328,
  NOTSRCCOPY = 0x00330008,
  NOTSRCERASE = 0x001100a6,
  MERGECOPY = 0x00c000ca,
  MERGEPAINT = 0x00bb0226,
  PATCOPY = 0x00f00021,
  PATPAINT = 0x00fb0a09,
  PATINVERT = 0x005a0049,
  DSTINVERT = 0x00550009,
  BLACKNESS = 0x00000042,
  WHITENESS = 0x00ff0062,
  CAPTUREBLT = 0x40000000, //only if WinVer >= 5.0.0 (see wingdi.h)
}
