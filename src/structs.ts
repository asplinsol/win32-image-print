/* eslint-disable node/no-extraneous-require */
/* eslint-disable node/no-extraneous-import */
import ffi from 'ffi-napi';
import refArrayDI from 'ref-array-di';
import { refType, types } from 'ref-napi';
import refStructDI from 'ref-struct-di';

export const StructType = refStructDI(require('ref-napi'));
export const ArrayType = refArrayDI(require('ref-napi'));

const uchar = types.uchar;
const short = types.short;
const ushort = types.ushort;
const ulong = types.ulong;
const long = types.long;
const dword = types.ulong;
const word = types.ushort;
export const FXPT2DOT30 = types.long;

export const DEVMODE = StructType({
  dmDeviceName: ArrayType(uchar, 32),
  dmSpecVersion: ushort,
  dmDriverVersion: ushort,
  dmSize: ushort,
  dmDriverExtra: ushort,
  dmFields: ulong,
  dmSkip8: ArrayType(short, 8),
  dmColor: short,
  dmDuplex: short,
  dmYResolution: short,
  dmTTOption: short,
  dmCollate: short,
  dmFormName: ArrayType(uchar, 32),
  dmLogPixels: ushort,
  dmBitsPerPel: ulong,
  dmPelsWidth: ulong,
  dmPelsHeight: ulong,
  dmSkip10: ArrayType(ulong, 10)
});
export const PDEVMODE = refType(DEVMODE); // A pointer to the above struct

export const DOCINFOA = StructType({
  cbSize: ffi.types.int,
  lpszDocName: types.CString,
  lpszOutput: types.CString,
  lpszDatatype: types.CString,
  fwType: 'uint32'
});
export const PDOCINFOA = refType(DOCINFOA); // A pointer to the above struct

export const RECT = StructType({
  left: long,
  top: long,
  right: long,
  bottom: long
});
export const PRECT = refType(RECT); // A pointer to the above struct

export const SIZE = StructType({
  cx: long,
  cy: long
});
export const PSIZE = refType(SIZE); // A pointer to the above struct

export const POINT = StructType({
  x: long,
  y: long
});
export const PPOINT = refType(POINT); // A pointer to the above struct

export const CIEXYZ = StructType({
  ciexyzX: FXPT2DOT30,
  ciexyzY: FXPT2DOT30,
  ciexyzZ: FXPT2DOT30
});
export const CIEXYZTRIPLE = StructType({
  ciexyzRed: CIEXYZ,
  ciexyzGreen: CIEXYZ,
  ciexyzBlue: CIEXYZ
});
export const RGBQUAD = StructType({
  rgbBlue: uchar,
  rgbGreen: uchar,
  rgbRed: uchar,
  rgbReserved: uchar
});
export const BITMAPV5HEADER = StructType({
  bV5Size: dword,
  bV5Width: long,
  bV5Height: long,
  bV5Planes: word,
  bV5BitCount: word,
  bV5Compression: dword,
  bV5SizeImage: dword,
  bV5XPelsPerMeter: long,
  bV5YPelsPerMeter: long,
  bV5ClrUsed: dword,
  bV5ClrImportant: dword,
  bV5RedMask: dword,
  bV5GreenMask: dword,
  bV5BlueMask: dword,
  bV5AlphaMask: dword,
  bV5CSType: dword,
  bV5Endpoints: CIEXYZTRIPLE,
  bV5GammaRed: dword,
  bV5GammaGreen: dword,
  bV5GammaBlue: dword,
  bV5Intent: dword,
  bV5ProfileData: dword,
  bV5ProfileSize: dword,
  bV5Reserved: dword
});
export const BITMAPINFO = StructType({
  bmiHeader: BITMAPV5HEADER,
  bmiColors: RGBQUAD
});
export const PBITMAPV5HEADER = refType(BITMAPV5HEADER); // A pointer to the above struct

export const BITMAPINFOHEADER = StructType({
  biSize: dword,
  biWidth: long,
  biHeight: long,
  biPlanes: word,
  biBitCount: word,
  biCompression: dword,
  biSizeImage: dword,
  biXPelsPerMeter: long,
  biYPelsPerMeter: long,
  biClrUsed: dword,
  biClrImportant: dword
});
export const PBITMAPINFOHEADER = refType(BITMAPINFOHEADER); // A pointer to the above struct
