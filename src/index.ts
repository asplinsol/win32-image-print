import { disposePrinter, EndDoc, endPage, getPrinterAsync, startDoc, startPage, writeImageFromBuffer } from './gdiAPI';

export type JOBRef = number

export async function printImage(imageBuffer: Buffer, printerName: string, printJobName?: string): Promise<JOBRef> {
    return new Promise((resolve, reject) => {
        getPrinterAsync(printerName, (err, hndl) => {
                if (err) reject(err);
                const jobRef = startDoc(hndl, printJobName || 'node-print', 'RAW');
                startPage(hndl);        
                void writeImageFromBuffer(hndl, imageBuffer).then(() => {
                    endPage(hndl);
                    EndDoc(hndl);
                    disposePrinter(hndl);
                    return resolve(jobRef);
                });
            
        });
    });
}
