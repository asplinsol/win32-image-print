import ffi from 'ffi-napi';
// eslint-disable-next-line node/no-extraneous-import
import { NULL, types } from 'ref-napi';
import {
  BITMAPINFOHEADER,
  DOCINFOA,
  PBITMAPINFOHEADER,
  PDEVMODE,
  PDOCINFOA,
  PPOINT,
  PSIZE,
} from './structs';
import {
  DeviceCap,
  DIBColors,
  MAPMODE,
  TernaryRasterOperations,
} from './types';
import Jimp from 'jimp';

export const HANDLE = 'uint32';

export const k32 = ffi.Library('Kernel32', {
  MulDiv: [ 'int', ['int','int','int']]
})
export const gdi32 = ffi.Library('gdi32', {
  CreateCompatibleDC: [HANDLE, [HANDLE]], // https://docs.microsoft.com/en-us/windows/win32/api/wingdi/nf-wingdi-createcompatibledc
  CreateCompatibleBitmap: [HANDLE, [HANDLE, 'int32', 'int32']], // https://docs.microsoft.com/en-us/windows/win32/api/wingdi/nf-wingdi-createcompatiblebitmap
  //'GetPixel': [ColorRefStruct, [HANDLE, 'int32', 'int32']], // https://docs.microsoft.com/en-us/windows/win32/api/wingdi/nf-wingdi-getpixel
  SelectObject: [HANDLE, [HANDLE, HANDLE]], // https://docs.microsoft.com/en-us/windows/win32/api/wingdi/nf-wingdi-selectobject
  //'GetObjectA': ['int32', [HANDLE, 'int32', ref.refType(BitmapStruct)]], // https://docs.microsoft.com/en-us/windows/win32/api/wingdi/nf-wingdi-getobjecta
  DeleteObject: ['bool', [HANDLE]], // https://www.pinvoke.net/default.aspx/gdi32.DeleteObject
  StartDocA: ['int', [HANDLE, PDOCINFOA]],
  CreateDCA: [HANDLE, [types.CString, types.CString, types.CString, PDEVMODE]],
  DeleteDC: ['bool', [HANDLE]],
  GetDeviceCaps: ['int', [HANDLE, 'int']],
  SetMapMode: ['int', [HANDLE, 'int']],
  StartPage: ['int', [HANDLE]],
  EndPage: ['int', [HANDLE]],
  EndDoc: ['int', [HANDLE]],
  SetViewportExtEx: ['bool', [HANDLE, 'int', 'int', PSIZE]],
  SetWindowExtEx: ['bool', [HANDLE, 'int', 'int', PSIZE]],
  SetWindowOrgEx: ['bool', [HANDLE, 'int', 'int', PPOINT]],
  SetDIBits: [ 'int32', [HANDLE, HANDLE, 'uint', 'uint', 'pointer', PBITMAPINFOHEADER, 'uint']],
  BitBlt: [ 'bool', [HANDLE, 'int', 'int', 'int', 'int', HANDLE, 'int', 'int', 'ulong']],
  StretchBlt: [ 'bool', [ HANDLE,  'int', 'int', 'int', 'int', HANDLE, 'int', 'int', 'int', 'int', 'ulong' ]], //https://docs.microsoft.com/en-us/windows/win32/api/wingdi/nf-wingdi-stretchblt
});

export type Size = {
  width: number;
  height: number;
  offsetX: number;
  offsetY: number;
  hResolution: number;
  vResolution: number;
};
export function getPrinter(name: string): number {
  return gdi32.CreateDCA(NULL as any, name, NULL as any, NULL as any);
}

export function getPrinterAsync(name: string, cb: (err: any, value: number) => void): void {
  gdi32.CreateDCA.async(NULL as any, name, NULL as any, NULL as any, cb);
}
export function disposePrinter(handle: number): boolean {
  return gdi32.DeleteDC(handle);
}
export function getPaperSize(handle: number): Size {
  return {
    width: gdi32.GetDeviceCaps(handle, DeviceCap.PHYSICALWIDTH),
    height: gdi32.GetDeviceCaps(handle, DeviceCap.PHYSICALHEIGHT),
    offsetX: gdi32.GetDeviceCaps(handle, DeviceCap.PHYSICALOFFSETX),
    offsetY: gdi32.GetDeviceCaps(handle, DeviceCap.PHYSICALOFFSETY),
    hResolution: gdi32.GetDeviceCaps(handle, DeviceCap.HORZRES),
    vResolution: gdi32.GetDeviceCaps(handle, DeviceCap.VERTRES),
  };
}
export function startDoc(
  handle: number,
  printJobFilename: string,
  dataType: 'RAW' | 'XPS_PASS' | 'EMF'
): number {
  const info = new DOCINFOA();
  info.lpszDocName = printJobFilename;
  info.lpszOutput = NULL as any;
  info.lpszDatatype = dataType;
  info.cbSize = DOCINFOA.size + printJobFilename.length + dataType.length;
  return gdi32.StartDocA(handle, info.ref());
}
export function EndDoc(handle: number): number {
  return gdi32.EndDoc(handle);
}
export function startPage(handle: number): number {
  return gdi32.StartPage(handle);
}
export function endPage(handle: number): number {
  return gdi32.EndPage(handle);
}
export async function writeImageFromBuffer(
  handle: number,
  imageBuffer: Buffer
): Promise<void> {
  const imageObj = await Jimp.read(imageBuffer);
  //imageObj.
  //console.log(imageObj);
  const { width, height } = imageObj.bitmap;

  const { hResolution, vResolution } = getPaperSize(handle);
  const isPrinterLandscape = hResolution > vResolution;
  const isImageLandscape = width > height;
  if (isPrinterLandscape && !isImageLandscape) {
    console.log('rotate');
  } else if (!isPrinterLandscape && isImageLandscape) {
    console.log('rotate');
  }
 
  gdi32.SetMapMode(handle, MAPMODE.ISOTROPIC);
  gdi32.SetViewportExtEx(handle, hResolution, vResolution, NULL as any);
  gdi32.SetWindowExtEx(handle, width, height, NULL as any);

  const bmpBuffer = await imageObj.getBufferAsync(Jimp.MIME_BMP);
  const offsetForBytes = bmpBuffer.readUInt32LE(0x0a);
  const bmHeader = new BITMAPINFOHEADER(bmpBuffer.slice(0x0e));
  const bmBits = bmpBuffer.slice(offsetForBytes);

  const memDC = gdi32.CreateCompatibleDC(handle);
  const bmHndl = gdi32.CreateCompatibleBitmap(handle,width, height);
  gdi32.SetDIBits( handle, bmHndl, 0, height, bmBits as any,  bmHeader.ref(), DIBColors.DIB_RGB_COLORS  );
  gdi32.SelectObject(memDC, bmHndl);

  gdi32.StretchBlt(handle,  0, 0,  width, height, memDC, 0, 0,  width, height, TernaryRasterOperations.SRCCOPY );
  gdi32.DeleteObject(bmHndl);
  gdi32.DeleteDC(memDC);
}

